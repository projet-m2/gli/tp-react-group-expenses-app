import React, { Component } from 'react';
import './App.css';
import ListShopping from './ListShopping';
import FilterUser from './FilterUser';
import AddPurchase from './AddPurchase';
import TotalShopping from './TotalShopping';

class App extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      costs: [
        { paidBy: 0, paidFor: 'Beer', amount: 15 },
        { paidBy: 0, paidFor: 'Cookies', amount: 4 },
        { paidBy: 1, paidFor: 'Pizza', amount: 10 },
        { paidBy: 1, paidFor: 'Chips', amount: 4 },
        { paidBy: 2, paidFor: 'Chocolate', amount: 3 },
        { paidBy: 2, paidFor: 'BBQ', amount: 20 },
        { paidBy: 3, paidFor: 'Beer', amount: 18 },
        { paidBy: 3, paidFor: 'Vegetable', amount: 5 }
      ],
      users:[
        'Toto',
        'Tata',
        'Titi',
        'Tutu'
      ],
      filterUser: -1
    }
  }

  filterUser = (paidBy) =>
    this.setState({filterUser: paidBy});

  addPurchase = (who, what, price) => {
    if(this.getUserIndex(who) == -1){
      this.setState({users: [...this.state.users, who]})
      this.setState({costs: [...this.state.costs, { paidBy: this.state.users.length, paidFor: what, amount: parseInt(price) }]});
    }
    else{
      this.setState({costs: [...this.state.costs, { paidBy: this.getUserIndex(who), paidFor: what, amount: parseInt(price) }]});
    }
  }

  getUserIndex = (user) => {
    for(var index = 0; index < this.state.users.length; index++) {
      if(this.state.users[index] === user) {
          return index;
      }
    }
    return -1;
  }


  render(){
    return(
    <div>
      <div class="App">
        <header class="App-header">
          <h1>Week end Party</h1>
        </header>
      </div>
      <FilterUser users={this.state.users} filterUser={this.filterUser}/>
      <ListShopping users={this.state.users} costs={this.state.costs} filterUser={this.state.filterUser}/>
      
      <footer>
      <AddPurchase addPurchase={this.addPurchase}/>
      <TotalShopping costs={this.state.costs} filterUser={this.state.filterUser}/>
      </footer>
      
    </div>
    );
  }

}

export default App;
