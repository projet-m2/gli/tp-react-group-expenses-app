import React, { Component } from 'react'

class TotalShopping extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
    let totalShop = this.props.costs
    .filter(cost => cost.paidBy == this.props.filterUser || this.props.filterUser == -1)
    .reduce((total, value) => total + value.amount, 0)

    return (

            <p>Total : {totalShop}€</p>
    );
    }

}

export default TotalShopping;