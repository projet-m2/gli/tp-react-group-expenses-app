import React, { Component } from 'react'

class FilterUser extends React.Component{

    constructor(props){
        super(props);
    }

    listUser = () =>
    this.props.users
    .map((user, index) => 
        <option value={index}>{user}</option>
    );

    handleChange(event){
        this.props.filterUser(event.target.value);
    }

    render(){
        return( 
        <form onChange={(event) => this.handleChange(event)}>
            <select>
              <option value="-1">All</option>
              {this.listUser()}
            </select>
        </form>  
        );
    }

}

export default FilterUser;
