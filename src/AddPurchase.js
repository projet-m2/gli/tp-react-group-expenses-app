import React, { Component } from 'react'

class AddPurchase extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            who: '',
            what: '',
            price: 0
        }
    }

    handleClick = (event) => {
        if(this.state.who != '' && this.state.what != '' && this.state.price > 0){
            this.props.addPurchase(this.state.who, this.state.what, this.state.price);
        }
    }

    
    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value })
    }

    render(){
        return (
        <form class="form-inline">
            <input type="text" name="who" placeholder="Paid by ?" onChange={event => this.handleChange(event)} required/>
        
            <input type="text" name="what" placeholder="Paid for ?" onChange={event => this.handleChange(event)} required/>
        
            <input type="number" name="price" placeholder="Amount ?" onChange={event => this.handleChange(event)} required/>
        
            <button type="button" onClick={this.handleClick}>Add</button>
        </form>
        );
    }
}
export default AddPurchase;